package com.vitatrade.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.vitatrade.vitatradeplatform.OrderService;
import com.vitatrade.vitatradeplatform.StockService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTests {

	@Autowired
	OrderService orderServ;
	
	@Autowired
	StockService stockServ;
	
	@Test
	public void testCreateNewOrder() {
		
		stockServ.createNewstock("a", "stock");
		orderServ.createNewOrder(true, 20, 100, "a", 0);
		assertEquals(1, orderServ.getTotalOrderCount());
	}
	
}
