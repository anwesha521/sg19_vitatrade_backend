package com.vitatrade.entity;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.vitatrade.vitatradeplatform.OrderStatus;
import com.vitatrade.vitatradeplatform.OrderStatusService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderStatusServiceTests {

	@Autowired
	OrderStatusService service;
	
	@Test
	public void testGetOrderStatusByDescription_filled() {
		OrderStatus os2 = service.getOrderStatusByDescription("FILLED");
		assertEquals(2, os2.getStatusId());
	}
	
	@Test
	public void testGetOrderStatusByDescription_partiallyFilled() {
		OrderStatus os3 = service.getOrderStatusByDescription("PARTIALLY_FILLED");
		assertEquals(3, os3.getStatusId());
	}
	
	@Test
	public void testGetOrderStatusByDescription_rejected() {
		OrderStatus os4 = service.getOrderStatusByDescription("REJECTED");
		assertEquals(4, os4.getStatusId());
	}
	
	@Test
	public void testGetInitialOrderStatus() {
		OrderStatus os1 = service.getInitialOrderStatus();
		assertEquals(1, os1.getStatusId());
	}

}
