package com.vitatrade.entity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.vitatrade.vitatradeplatform.Stock;
import com.vitatrade.vitatradeplatform.StockService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StockServiceTests {

	@Autowired
	StockService service;
	
	private int numOfStocks = 0;

	@Test
	public void testGetAllStocks_beforeFetchingFeed() {
		assertThat(service.getAllStocks()).hasSize(0);
	}
	
	@Test
	public void testCreateNewStocks() {
		service.createNewstock("1234", "abc company");
		Stock s = service.getStockByCode("1234");
		assertEquals("abc company", s.getStockName());
	}
	
	
//	@Test
//	public void testGetAllStocks_afterFetchingFeed() {
//		service.fetchStocksFromFeed();
//		numOfStocks += 526;
//		assertThat(service.getAllStocks()).hasSize(numOfStocks+1);
//	}
	
	
}
