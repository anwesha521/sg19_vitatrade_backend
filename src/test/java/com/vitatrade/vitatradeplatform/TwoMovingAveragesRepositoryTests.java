package com.vitatrade.vitatradeplatform;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TwoMovingAveragesRepositoryTests {

	
	@Autowired
	private TwoMovingAveragesRepository twoMovingRepo;
	
	@Autowired
	private StockRepository stockRepo;
	@Autowired
	private StockService stockServ;
	

	@Test
	public void testRoiAfterCreatingEntity() {	
		stockServ.createNewstock("a", "apple");
		TwoMovingAveragesEntity entInst=new TwoMovingAveragesEntity(stockRepo.getStockByCode("a"),10,30,1,0,100,300);   
		twoMovingRepo.addStrategy(entInst);
		assert(entInst.getRoi()==0);
	}

	@Test
	public void testNumberOfActive() {
		stockServ.createNewstock("aa", "apple ltd");
		TwoMovingAveragesEntity entInst=new TwoMovingAveragesEntity(stockRepo.getStockByCode("aa"),10,30,1,1,100,300);   
		twoMovingRepo.addStrategy(entInst);
		stockServ.createNewstock("goog", "google");
		TwoMovingAveragesEntity entnst1=new TwoMovingAveragesEntity(stockRepo.getStockByCode("goog"),10,30,1,2,200,500);   
		twoMovingRepo.addStrategy(entnst1);
		assert(twoMovingRepo.getActiveStrategy().size()==2);
	   
	}
}
