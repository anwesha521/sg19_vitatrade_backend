package com.vitatrade.vitatradeplatform;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ORDERBOOK")
public class Order {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int order_id = -1;
	
	private boolean isbuy;
	private double price;
	private int initial_size;
	private int approved_size;
	

	private double total;
	private Timestamp order_time;
	
	@ManyToOne
	@JoinColumn(name = "STOCK_CODE")
	private Stock stock;
	
	@ManyToOne
	@JoinColumn(name = "STATUS_ID")
	private OrderStatus status;
	
//	@ManyToOne
//	@JoinColumn(name = "STRATEGY_ID")
	private int strategy_id;
	
	public Order() {
	}
	
	public Order(Boolean isBuy, double price, int size, Stock stock, OrderStatus status, int strategyId) {
		this.setBuy(isBuy);
		this.setPrice(price);
		this.setInitial_size(size);
		this.setTotal(0);
		this.setOrder_time(new Timestamp(System.currentTimeMillis()));
		this.stock = stock;
		this.setOrderStatus(status);
		this.strategy_id = strategyId;
	}

	public int getOrderId() {
		return order_id;
	}
	
	public boolean isBuy() {
		return isbuy;
	}

	public void setBuy(boolean isBuy) {
		this.isbuy = isBuy;
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}


	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Timestamp getOrder_time() {
		return order_time;
	}

	public void setOrder_time(Timestamp order_time) {
		this.order_time = order_time;
	}
	
	public Stock getStock() {
		return stock;
	}
	
	public void setStock(Stock stock) {
		this.stock = stock;
	}
	
	public OrderStatus getOrderStatus() {
		return status;
	}
	
	public void setOrderStatus(OrderStatus status) {
		this.status = status;
	}
	
	public int getStrategyId() {
		return strategy_id;
	}
	
	public void setStrategyId(int strategyId) {
		this.strategy_id = strategyId;
	}

	public int getInitial_size() {
		return initial_size;
	}

	public void setInitial_size(int initial_size) {
		this.initial_size = initial_size;
	}

	public int getApproved_size() {
		return approved_size;
	}

	public void setApproved_size(int approved_size) {
		this.approved_size = approved_size;
	}
	
	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}
	
}
