package com.vitatrade.vitatradeplatform;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STOCK")
public class Stock {

	@Id
	private String stockCode;
	private String stockName;
	
	public Stock() {
		
	}
	
	public Stock(String stockCode, String stockName) {
		this.setStockCode(stockCode);
		this.setStockName(stockName);
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	
	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	
	
}
