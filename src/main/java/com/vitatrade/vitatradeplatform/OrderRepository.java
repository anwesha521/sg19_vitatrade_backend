package com.vitatrade.vitatradeplatform;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class OrderRepository {

	@PersistenceContext(name="myPersistenceUnit")
	protected EntityManager entityManager;
	
	public long getTotalOrderCount() throws DataAccessException {
		String jpql = "select count(o) from Order o";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		return query.getSingleResult();
	}
	
	public Order getOrder(int orderId) throws DataAccessException {
	    return entityManager.find(Order.class, orderId);
	}
	
	public List<Order> getLatestOrders() throws DataAccessException {
		String jpql = "select o from Order o";
		TypedQuery<Order> query = entityManager.createQuery(jpql, Order.class);
		query.setMaxResults(100);
	    return query.getResultList();
	}
	
	@Transactional
	public Order insertNewOrder(Order o) {
		entityManager.persist(o);
		return o;
	}
	
	@Transactional
	public void updateOrderStatus(Order o, OrderStatus os) {
		Order entity = entityManager.find(Order.class, o.getOrderId());
		entity.setOrderStatus(os);
	}
	
	@Transactional
	public void updateOrderApprovedSize(Order o, int approvedSize) {
		Order entity = entityManager.find(Order.class, o.getOrderId());
		entity.setApproved_size(approvedSize);
		entity.setTotal(o.getPrice()*approvedSize);
	}
	
	@Transactional
	public boolean deleteOrder(int orderId) {
		Order o = entityManager.find(Order.class, orderId);
		if(o != null) {
			entityManager.remove(o);
			return true;
		}else
			return false;
		
	}
}
