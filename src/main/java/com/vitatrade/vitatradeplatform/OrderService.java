package com.vitatrade.vitatradeplatform;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderService {

	@Autowired
	private OrderRepository orderRepo;

	@Autowired
	private OrderStatusRepository statusRepo;
	@Autowired
	private StockRepository stockRepo;
	
	/*
	public List<Order> getOrdersByStrategy(int strategyId){
//		Strategy strategy = strategyRepo.getStrategy(strategyId);
		return orderRepo.getOrdersByStrategy(strategy);
	}
	*/
	

	public List<Order> getLatestOrders(){
		return orderRepo.getLatestOrders();
	}
	
	public long getTotalOrderCount() {
		long orderCount = orderRepo.getTotalOrderCount();
		System.out.printf("Order count: %d\n", orderCount);
		return orderCount;
	}
	
	public Order createNewOrder(boolean isBuy, double price, int size, String stockCode, int strategyId) {

		OrderStatus initialStatus = statusRepo.getPendingStatus();
		Stock stock = stockRepo.getStockByCode(stockCode);
		Order newOrder = new Order(isBuy, price, size, stock, initialStatus, strategyId);
		return orderRepo.insertNewOrder(newOrder);
	}
	
	public void updateOrderStatus(Order o, String status) {
		OrderStatus os = statusRepo.getOrderStatusByDescription(status);
		orderRepo.updateOrderStatus(o, os);
	}
	
	public void updateOrderApprovedSize(Order o, int approvedSize) {
		orderRepo.updateOrderApprovedSize(o, approvedSize);
	}

	public boolean deleteOrder(int orderId) {
		return orderRepo.deleteOrder(orderId);
	}
}
