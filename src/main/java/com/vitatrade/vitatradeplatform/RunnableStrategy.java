package com.vitatrade.vitatradeplatform;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.vitatrade.brokerservice.OrderSender;

public abstract class RunnableStrategy implements Runnable {
	protected int stratID;
	protected String stockID;
	protected int sizePerTrade;
	protected int maximumSize = 0;
	protected double currentPrice = 0.0;
	protected volatile double unrealisedLongAmount = 0.0;
	protected volatile int unrealisedLongVolume = 0;
	protected volatile double unrealisedShortAmount = 0.0;
	protected volatile int unrealisedShortVolume = 0;
	protected volatile double realisedLongAmount = 0.0;
	protected volatile int realisedLongVolume = 0;
	protected volatile double realisedShortAmount = 0.0;
	protected volatile int realisedShortVolume = 0;
	protected volatile boolean isActive = true;
	protected Map<String, Order> orderList = new HashMap<>();
	protected ApplicationContext applicationContext;
	protected FeedRestClient feedRestClient;
	protected OrderSender orderSender;	
	protected OrderStatusService orderStatusService;
	protected OrderService orderServ;
	
	public abstract void run();
	
	public void sendOrder(String option, int size, double price) {
		System.out.println("sending "+option+size);///
		boolean isBuy=false;
		if(option.equals("buy")) {
			unrealisedLongVolume += size;
			unrealisedLongAmount += size * price;
			isBuy=true;
		}
		else {
			unrealisedShortVolume += size;
			unrealisedShortAmount += size * price;
		}
		
		Order newOrder = orderServ.createNewOrder(isBuy, price, size,stockID,stratID); 
		String corrid = orderSender.sendOrderToBroker(newOrder);
		System.out.println("After sending order");
		orderList.put(corrid, newOrder);
	}
	
	public void receiveReply(String corrID, String status, String option,double price,int volume) {
		if (!orderList.containsKey(corrID)) {
			return;
		}
		
		orderServ.updateOrderStatus(orderList.get(corrID), status);
		orderServ.updateOrderApprovedSize(orderList.get(corrID), volume);
		
		if(status.equals("FILLED")) {
			if(option.equals("buy")) {
				realisedLongVolume += volume;
				realisedLongAmount += volume * price;
				unrealisedLongVolume -= volume;
				unrealisedLongAmount -= volume * price;
			} else {
				realisedShortVolume += volume;
				realisedShortAmount += volume * price;
				unrealisedShortVolume -= volume;
				unrealisedShortAmount -= volume * price;
			}
		} else if(status.equals("REJECTED")) {
			if(option.equals("buy")) {
				unrealisedLongVolume -= sizePerTrade;
				unrealisedLongAmount -= sizePerTrade * price;
				sendOrder("buy", sizePerTrade, price);
			} else {
				unrealisedShortVolume -= sizePerTrade;
				unrealisedShortAmount -= sizePerTrade * price;
				sendOrder("sell", sizePerTrade, price);
			}
		} else if(status.equals("PARTIALLY_FILLED")) {
			if(option.equals("buy")) {
				realisedLongVolume += volume;
				realisedLongAmount += volume * price;
				unrealisedLongVolume -= volume;
				unrealisedLongAmount -= volume * price;
				sendOrder("buy", sizePerTrade - volume, price);
			} else {
				realisedShortVolume += volume;
				realisedShortAmount += volume * price;
				unrealisedShortVolume -= volume;
				unrealisedShortAmount -= volume * price;
				sendOrder("sell", sizePerTrade - volume, price);
			}
		}
	}
	
	public void forceStop() {
		isActive = false;
	}
	
	public void prepareToRestart () {
		isActive = true;
		unrealisedLongAmount = 0.0;
		unrealisedLongVolume = 0;
		unrealisedShortAmount = 0.0;
		unrealisedShortVolume = 0;
		realisedLongAmount = 0.0;
		realisedLongVolume = 0;
		realisedShortAmount = 0.0;
		realisedShortVolume = 0;
	}

	protected boolean needtoExit() {
		if (isActive == false) {
			return true;
		}
		
		if (noPendingOrder()) {
			//case1
			if(realisedLongVolume - realisedShortVolume == 0 && 
					outOfRange(realisedShortAmount - realisedLongAmount, realisedLongAmount)) {
				System.out.println("Exit due to Case 1");
				
				return true;
			} else if (realisedLongVolume - realisedShortVolume != 0) {
				//case 3
				if(realisedShortVolume - realisedLongVolume < 0) {
					double d = -(realisedShortVolume - realisedLongVolume) * currentPrice + realisedShortAmount - realisedLongAmount; 
					if (outOfRange(d, realisedLongAmount)) {
						sendOrder("sell", realisedLongVolume - realisedShortVolume, currentPrice);
						System.out.println("Exit due to Case 3");
						return true;
					}
				}else if (realisedLongVolume - realisedShortVolume < 0) {
					double d = (realisedLongVolume - realisedShortVolume) * currentPrice + realisedShortAmount - realisedLongAmount;
					if (outOfRange (d , (realisedLongAmount + (realisedLongVolume - realisedShortVolume) * currentPrice))) {
						sendOrder("sell", realisedShortVolume - realisedLongVolume, currentPrice);
						System.out.println("Exit due to Case 3");
						return true;
					}
				}
			}
		}
		//case 2
		if (unrealisedLongVolume + realisedLongVolume - unrealisedShortVolume - realisedShortVolume == 0
				&& outOfRange((realisedShortAmount - realisedLongAmount + unrealisedShortAmount - unrealisedLongAmount) 
						, (realisedLongAmount + unrealisedLongAmount))) {
			
			System.out.println("Exit due to Case 2");
			
			return true;
		}
		return false;
	}
	
	protected boolean outOfRange(double d1, double d2) {
		return d1 > d2 * 0.01 || d1 < d2 * -0.01;
	}
	
	protected boolean noPendingOrder() {
		return unrealisedLongVolume == 0 && unrealisedShortVolume == 0;
	}
	
	protected boolean longOverMaxSize() {
		return unrealisedLongVolume + realisedLongVolume - unrealisedShortVolume - realisedShortVolume > maximumSize;
	}
	
	protected boolean shortOverMaxSize() {
		return unrealisedShortVolume + realisedShortVolume - unrealisedLongVolume - realisedLongVolume > maximumSize;
	}
	
}
