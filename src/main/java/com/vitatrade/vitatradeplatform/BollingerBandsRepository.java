package com.vitatrade.vitatradeplatform;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class BollingerBandsRepository {
	
	@PersistenceContext(name="myPersistenceUnit")
	protected EntityManager entityManager;
	
	public BollingerBandsEntity getStrategy(int stratID) {
		BollingerBandsEntity entity = entityManager.find(BollingerBandsEntity.class, stratID);
		return entity;
	}
	
	@Transactional
	public BollingerBandsEntity addStrategy(BollingerBandsEntity entInst) {
		entityManager.persist(entInst);
		return entInst;
	}

	@Transactional
	public void setEndTime(BollingerBandsEntity entInst, long time) {
		BollingerBandsEntity entity = entityManager.find(BollingerBandsEntity.class, entInst.getStratID());
		entity.setEndTime(time);
	}
	
	@Transactional
	public void setStartTime(BollingerBandsEntity entInst, long time) {
		BollingerBandsEntity entity = entityManager.find(BollingerBandsEntity.class, entInst.getStratID());
		entity.setStartTime(time);
	}
	
	@Transactional
	public void changeActiveStatus(BollingerBandsEntity entInst, boolean status) {
		BollingerBandsEntity entity = entityManager.find(BollingerBandsEntity.class, entInst.getStratID());
		entity.setActive(status);
	}
	
	@Transactional
	public void changeSleepStatus(BollingerBandsEntity entInst, boolean status) {
		BollingerBandsEntity entity = entityManager.find(BollingerBandsEntity.class, entInst.getStratID());
		entity.setSleepStatus(status);
	}
	
	@Transactional
	public void updatePnl(BollingerBandsEntity entInst, double pnl) {
		BollingerBandsEntity entity = entityManager.find(BollingerBandsEntity.class, entInst.getStratID());
		entity.setPnl(pnl);
	}
	
	@Transactional
	public void updateRoI(BollingerBandsEntity entInst, double roi) {
		BollingerBandsEntity entity = entityManager.find(BollingerBandsEntity.class, entInst.getStratID());
		entity.setRoi(roi);
	}
	
	public List<BollingerBandsEntity> getActiveStrategy() throws DataAccessException {
		String jpql = "select s from BollingerBandsEntity s where isActive=true";
		TypedQuery<BollingerBandsEntity> query = entityManager.createQuery(jpql, BollingerBandsEntity.class);
		query.setMaxResults(100);
	    return query.getResultList();
	}
	
	public List<BollingerBandsEntity> getAllStrategy() throws DataAccessException {
		String jpql = "select s from BollingerBandsEntity s";
		TypedQuery<BollingerBandsEntity> query = entityManager.createQuery(jpql, BollingerBandsEntity.class);
		query.setMaxResults(100);
		System.out.println("QUERYING PRICE BREAKOUT");
		System.out.println(query.getResultList());
	    return query.getResultList();
	}
}
