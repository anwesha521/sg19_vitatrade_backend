package com.vitatrade.vitatradeplatform;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@ComponentScan({ "com.vitatrade.vitatradeplatform", "com.vitatrade.entity" })
@RestController
public class UIRestController {

	@Autowired
	StockService stockService;

	@Autowired
	FeedRestClient feedClient;

	@Autowired
	RunStrategyManager strategyManager;

	@Autowired
	OrderService orderService;

	@Autowired
	TwoMovingAveragesRepository movingAverageRepo;

	@Autowired
	PriceBreakoutRepository priceBreakoutRepo;
	
	@Autowired
	BollingerBandsRepository bollingerBandsRepo;

	@GetMapping("/orderHistory")
	public ResponseEntity<List<Order>> getOrderHistory() {
		return new ResponseEntity<>(orderService.getLatestOrders(), HttpStatus.OK);
	}

	@GetMapping("/stocks")
	public ResponseEntity<List<Stock>> getStocks() {
		boolean stocksNotFetched = stockService.getAllStocks().size() == 0;
		System.out.println("stocks Get" + stocksNotFetched);
		if (stocksNotFetched) {
			System.out.println("stocks FETCHING..." + stocksNotFetched);
			stockService.fetchStocksFromFeed();
		}
		return new ResponseEntity<>(stockService.getAllStocks(), HttpStatus.OK);

	}

	@GetMapping("/livefeed")
	public ResponseEntity<String> getLiveFeed(@RequestParam("stock") String stock) {
		boolean priceNotFetched = feedClient.getPriceforGraph(stock).equals(null);
		System.out.println("price fetched:" + priceNotFetched);
		return new ResponseEntity<>(feedClient.getPriceforGraph(stock), HttpStatus.OK);

	}

	// start a strategy
	@RequestMapping(method = RequestMethod.POST, value = "/start", headers = {
			"Content-Type=application/json, application/xml, text/plain",
			"Accept=application/json, application/xml, text/plain" })
	@ResponseStatus(HttpStatus.CREATED) // define HTTP response
	public JSONObject addItem(@RequestBody String strategyString) { // received object
		System.out.println("in" + strategyString);
		JSONObject strategy = new JSONObject(strategyString);
		System.out.println("strategy converted \n" + strategy);
		try {
			strategyManager.startStrategy(strategy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strategy;
	}

	@GetMapping("/movingAverageStrats")
	public ResponseEntity<List<TwoMovingAveragesEntity>> getMovingAvgStrategies() {
		System.out.println("GETTING MOVING AVERAGE STRATEGIES");
		return new ResponseEntity<>(movingAverageRepo.getAllStrategy(), HttpStatus.OK);
	}

	@GetMapping("/priceBreakoutStrats")
	public ResponseEntity<List<PriceBreakoutEntity>> getPriceBreakoutStrategies() {
		return new ResponseEntity<>(priceBreakoutRepo.getAllStrategy(), HttpStatus.OK);
	}
	
	@GetMapping("/bollingerBandsStrats")
	public ResponseEntity<List<BollingerBandsEntity>> getBollingerBandsStrategies() {
		return new ResponseEntity<>(bollingerBandsRepo.getAllStrategy(), HttpStatus.OK);
	}

	// stop a strategy
	@RequestMapping(method = RequestMethod.POST, value = "/stop", headers = {
			"Content-Type=application/json, application/xml, text/plain",
			"Accept=application/json, application/xml, text/plain" })
	@ResponseStatus(HttpStatus.CREATED) // define HTTP response
	public Integer stopStrategy(@RequestBody String strategyString) { // received object
		System.out.println("force stopping strategy #" + strategyString);
		Integer strategyID = -1;
		try {
			strategyID = Integer.parseInt(strategyString);
			strategyManager.stopStrategy(strategyID);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strategyID;
	}

	// restart a strategy
	@RequestMapping(method = RequestMethod.POST, value = "/restart", headers = {
			"Content-Type=application/json, application/xml, text/plain",
			"Accept=application/json, application/xml, text/plain" })
	@ResponseStatus(HttpStatus.CREATED) // define HTTP response
	public Integer restartStrategy(@RequestBody String strategyString) { // received object
		System.out.println("force stopping strategy #" + strategyString);
		Integer strategyID = -1;
		try {
			strategyID = Integer.parseInt(strategyString);
			strategyManager.restartStrategy(strategyID);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strategyID;
	}

}
