package com.vitatrade.vitatradeplatform;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class StockRepository {

	@PersistenceContext(name="myPersistenceUnit")
	protected EntityManager entityManager;
	
	@Transactional
	public void insertStock(Stock s) {
		entityManager.persist(s);
	}
	
	@Transactional
	public void deleteStock(Stock s) {
		entityManager.remove(s);
	}
	
	public Stock getStockByCode(String stockCode) {
		String jpql = "select s from Stock s where s.stockCode = :stockCode";
		TypedQuery<Stock> query = entityManager.createQuery(jpql, Stock.class);
		query.setParameter("stockCode", stockCode);
	    return query.getSingleResult();
	}
	
	public List<Stock> getAllStocks(){
		String jpql = "select s from Stock s";
		TypedQuery<Stock> query = entityManager.createQuery(jpql, Stock.class);
	    return query.getResultList();
	}
	
}
