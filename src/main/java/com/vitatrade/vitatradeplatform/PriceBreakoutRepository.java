package com.vitatrade.vitatradeplatform;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PriceBreakoutRepository {
	
	@PersistenceContext(name="myPersistenceUnit")
	protected EntityManager entityManager;
	
	public PriceBreakoutEntity getStrategy(int stratID) {
		PriceBreakoutEntity entity = entityManager.find(PriceBreakoutEntity.class, stratID);
		return entity;
	}
	
	@Transactional
	public PriceBreakoutEntity addStrategy(PriceBreakoutEntity entInst) {
		entityManager.persist(entInst);
		return entInst;
	}
	@Transactional
	public void setEndTime(PriceBreakoutEntity entInst, long time) {
		PriceBreakoutEntity entity = entityManager.find(PriceBreakoutEntity.class, entInst.getStratID());
		entity.setEndTime(time);
	}
	
	@Transactional
	public void setStartTime(PriceBreakoutEntity entInst, long time) {
		PriceBreakoutEntity entity = entityManager.find(PriceBreakoutEntity.class, entInst.getStratID());
		entity.setStartTime(time);
	}
	
	@Transactional
	public void changeActiveStatus(PriceBreakoutEntity entInst, boolean status) {
		PriceBreakoutEntity entity = entityManager.find(PriceBreakoutEntity.class, entInst.getStratID());
		entity.setActive(status);
	}
	
	@Transactional
	public void changeSleepStatus(PriceBreakoutEntity entInst, boolean status) {
		PriceBreakoutEntity entity = entityManager.find(PriceBreakoutEntity.class, entInst.getStratID());
		entity.setSleepStatus(status);
	}
	
	@Transactional
	public void updatePnl(PriceBreakoutEntity entInst, double pnl) {
		PriceBreakoutEntity entity = entityManager.find(PriceBreakoutEntity.class, entInst.getStratID());
		entity.setPnl(pnl);
	}
	
	@Transactional
	public void updateRoI(PriceBreakoutEntity entInst, double roi) {
		PriceBreakoutEntity entity = entityManager.find(PriceBreakoutEntity.class, entInst.getStratID());
		entity.setRoi(roi);
	}
	
	public List<PriceBreakoutEntity> getActiveStrategy() throws DataAccessException {
		String jpql = "select s from PriceBreakoutEntity s where isActive=true";
		TypedQuery<PriceBreakoutEntity> query = entityManager.createQuery(jpql, PriceBreakoutEntity.class);
		query.setMaxResults(100);
	    return query.getResultList();
	}
	
	public List<PriceBreakoutEntity> getAllStrategy() throws DataAccessException {
		String jpql = "select s from PriceBreakoutEntity s";
		TypedQuery<PriceBreakoutEntity> query = entityManager.createQuery(jpql, PriceBreakoutEntity.class);
		query.setMaxResults(100);
		System.out.println("QUERYING PRICE BREAKOUT");
		System.out.println(query.getResultList());
	    return query.getResultList();
	}

}
