package com.vitatrade.vitatradeplatform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderStatusService {

	@Autowired
	OrderStatusRepository osRepo;
	
	public OrderStatus getOrderStatusByDescription(String status) {
		return osRepo.getOrderStatusByDescription(status);
	}
	
	public OrderStatus getInitialOrderStatus() {
		return osRepo.getPendingStatus();
	}
}
