package com.vitatrade.vitatradeplatform;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="STRATEGY_PRICEBREAKOUT", schema="VITATRADESCHEMA")
public class PriceBreakoutEntity {
	
	@Id
	private int strategyId;
	
	@ManyToOne
	@JoinColumn(name = "STOCK_CODE")
	private Stock stock;
	private int windowPeriod;
	private int triggerThreshold;
	
	private int size;
	private int maxAmount;
	private double overallPnl;
	private double overallRoi;
	private boolean isactive;
	private Timestamp startTime;
	private Timestamp exitTime;
	
	private boolean isSleeping;
	
	public PriceBreakoutEntity() {}
	
	public PriceBreakoutEntity(Stock stock, int windowperiod,int threshold, int stratid,int vol, int max) {
		this.stock=stock;
		this.windowPeriod=windowperiod;
		this.triggerThreshold= threshold;
		this.isSleeping=false;
		this.strategyId=stratid;
		this.size=vol;
		this.maxAmount=max;
		this.isactive=true;
		this.startTime=new Timestamp(new Date().getTime());
		this.exitTime=null;
		this.overallPnl=0;
		this.overallRoi=0;
	}

	public int getStratID() {
		return strategyId;
	}

	public void setStratID(int stratID) {
		this.strategyId = stratID;
	}

	public Stock getStockID() {
		return stock;
	}

	public void setStockID(Stock stockID) {
		this.stock = stockID;
	}
	

	public boolean isSleepStatus() {
		return isSleeping;
	}

	public void setSleepStatus(boolean sleepStatus) {
		this.isSleeping = sleepStatus;
	}

	public int getWindowPeriod() {
		return windowPeriod;
	}

	public void setWindowPeriod(int windowPeriod) {
		this.windowPeriod = windowPeriod;
	}

	public int getTriggerThreshold() {
		return triggerThreshold;
	}

	public void setTriggerThreshold(int triggerThreshold) {
		this.triggerThreshold = triggerThreshold;
	}

	public int getVolume() {
		return size;
	}

	public void setVolume(int volume) {
		this.size = volume;
	}

	public int getMaxamt() {
		return maxAmount;
	}

	public void setMaxamt(int maxamt) {
		this.maxAmount = maxamt;
	}

	public double getPnl() {
		return overallPnl;
	}

	public void setPnl(double pnl) {
		this.overallPnl = pnl;
	}

	public double getRoi() {
		return overallRoi;
	}

	public void setRoi(double roi) {
		this.overallRoi = roi;
	}


	public boolean isActive() {
		return isactive;
	}

	public void setActive(boolean isActive) {
		this.isactive = isActive;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = new Timestamp(startTime);
	}

	public Timestamp getEndTime() {
		return exitTime;
	}

	public void setEndTime(long endTime) {
		if (endTime == 0) {
			this.exitTime = null;
		} else {
			this.exitTime = new Timestamp(endTime);
		}
	}
	
	
	

}
