package com.vitatrade.vitatradeplatform;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class TwoMovingAveragesRepository {
	
	@PersistenceContext(name="myPersistenceUnit")
	protected EntityManager entityManager;
	
	public TwoMovingAveragesEntity getStrategy(int stratID) {
		TwoMovingAveragesEntity entity = entityManager.find(TwoMovingAveragesEntity.class, stratID);
		return entity;
	}
	
	@Transactional
	public TwoMovingAveragesEntity addStrategy(TwoMovingAveragesEntity entInst) {
		entityManager.persist(entInst);
		return entInst;
	}
	@Transactional
	public void setEndTime(TwoMovingAveragesEntity entInst, long time) {
		TwoMovingAveragesEntity entity = entityManager.find(TwoMovingAveragesEntity.class, entInst.getStratID());
		entity.setEndTime(time);
	}
	
	@Transactional
	public void setStartTime(TwoMovingAveragesEntity entInst, long time) {
		TwoMovingAveragesEntity entity = entityManager.find(TwoMovingAveragesEntity.class, entInst.getStratID());
		entity.setStartTime(time);
	}
	
	@Transactional
	public void changeActiveStatus(TwoMovingAveragesEntity entInst, boolean status) {
		TwoMovingAveragesEntity entity = entityManager.find(TwoMovingAveragesEntity.class, entInst.getStratID());
		entity.setActive(status);
	}
	
	@Transactional
	public void changeSleepStatus(TwoMovingAveragesEntity entInst, boolean status) {
		TwoMovingAveragesEntity entity = entityManager.find(TwoMovingAveragesEntity.class, entInst.getStratID());
		entity.setSleepStatus(status);
	}
	
	@Transactional
	public void updatePnl(TwoMovingAveragesEntity entInst, double pnl) {
		TwoMovingAveragesEntity entity = entityManager.find(TwoMovingAveragesEntity.class, entInst.getStratID());
		entity.setPnl(pnl);
	}
	@Transactional
	public void updateRoI(TwoMovingAveragesEntity entInst, double roi) {
		TwoMovingAveragesEntity entity = entityManager.find(TwoMovingAveragesEntity.class, entInst.getStratID());
		System.out.println("roi: " +roi);
		entity.setRoi(roi);
	}
	
	public List<TwoMovingAveragesEntity> getActiveStrategy() throws DataAccessException {
		String jpql = "select s from TwoMovingAveragesEntity s where isActive=true";
		TypedQuery<TwoMovingAveragesEntity> query = entityManager.createQuery(jpql, TwoMovingAveragesEntity.class);
		query.setMaxResults(100);
		System.out.println(query.getResultList());
		return query.getResultList();
	}
	
	public List<TwoMovingAveragesEntity> getAllStrategy() throws DataAccessException {
		String jpql = "select s from TwoMovingAveragesEntity s";
		TypedQuery<TwoMovingAveragesEntity> query = entityManager.createQuery(jpql, TwoMovingAveragesEntity.class);
		query.setMaxResults(100);
		System.out.println(query.getResultList());
		return query.getResultList();
	}
	

}
