package com.vitatrade.vitatradeplatform;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="STRATEGY_BOLLINGERBANDS", schema="VITATRADESCHEMA")
public class BollingerBandsEntity {
	@Id
	private int strategyId;
	
	@ManyToOne
	@JoinColumn(name = "STOCK_CODE")
	private Stock stock;
	
	private int averagePeriod;
	private int multipleOfSd;
	
	private int size;
	private int maxAmount;
	private double overallPnl;
	private double overallRoi;
	private boolean isactive;
	private Timestamp startTime;
	private Timestamp exitTime;
	private boolean isSleeping;
	
	public BollingerBandsEntity() {}
	
	public BollingerBandsEntity(Stock stock, int averagePeriod, int multipleOfSd, int stratid,int vol, int max) {
		this.stock=stock;
		this.averagePeriod = averagePeriod;
		this.multipleOfSd = multipleOfSd;
		this.isSleeping=false;
		this.strategyId=stratid;
		this.size=vol;
		this.maxAmount=max;
		this.isactive=true;
		this.startTime=new Timestamp(new Date().getTime());
		this.exitTime=null;
		this.overallPnl=0;
		this.overallRoi=0;
	}

	public int getStratID() {
		return strategyId;
	}

	public void setStratID(int strategyId) {
		this.strategyId = strategyId;
	}

	public Stock getStockID() {
		return stock;
	}

	public void setStockID(Stock stock) {
		this.stock = stock;
	}

	public int getAveragePeriod() {
		return averagePeriod;
	}

	public void setAveragePeriod(int averagePeriod) {
		this.averagePeriod = averagePeriod;
	}

	public int getMultipleOfSd() {
		return multipleOfSd;
	}

	public void setMultipleOfSd(int multipleOfSd) {
		this.multipleOfSd = multipleOfSd;
	}

	public int getVolume() {
		return size;
	}

	public void setVolume(int size) {
		this.size = size;
	}

	public int getMaxamt() {
		return maxAmount;
	}

	public void setMaxamt(int maxAmount) {
		this.maxAmount = maxAmount;
	}

	public double getPnl() {
		return overallPnl;
	}

	public void setPnl(double overallPnl) {
		this.overallPnl = overallPnl;
	}

	public double getRoi() {
		return overallRoi;
	}

	public void setRoi(double overallRoi) {
		this.overallRoi = overallRoi;
	}

	public boolean isActive() {
		return isactive;
	}

	public void setActive(boolean isactive) {
		this.isactive = isactive;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = new Timestamp(startTime);
	}

	public Timestamp getEndTime() {
		return exitTime;
	}

	public void setEndTime(long endTime) {
		if (endTime == 0) {
			this.exitTime = null;
		} else {
			this.exitTime = new Timestamp(endTime);
		}
	}

	public boolean isSleepStatus() {
		return isSleeping;
	}

	public void setSleepStatus(boolean isSleeping) {
		this.isSleeping = isSleeping;
	}
}
