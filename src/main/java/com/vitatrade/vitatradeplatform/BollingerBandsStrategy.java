package com.vitatrade.vitatradeplatform;

import java.util.ArrayList;
import java.util.Date;
import java.util.OptionalDouble;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.context.ApplicationContext;

import com.vitatrade.brokerservice.OrderSender;

public class BollingerBandsStrategy extends RunnableStrategy {
	private int averagePeriod;
	private int multipleOfSd;
	private BollingerBandsRepository bollingerBandsRepo;
	private BollingerBandsEntity bollingerBandsEnt;
	
	public BollingerBandsStrategy(int stratID, String stockID, int sizePerTrade, int maximumSize, int averagePeriod, 
			int multipleOfSd, ApplicationContext applicationContext, BollingerBandsRepository bollingerBandsRepo, 
			BollingerBandsEntity bollingerBandsEnt) {
		this.stratID = stratID;
		this.stockID = stockID;
		this.sizePerTrade = sizePerTrade;
		this.maximumSize = maximumSize;
		this.averagePeriod = averagePeriod;
		this.multipleOfSd = multipleOfSd;
		this.applicationContext = applicationContext;
		this.feedRestClient = applicationContext.getBean("feedRestClient", FeedRestClient.class);
		this.orderSender = applicationContext.getBean("orderSender", OrderSender.class);
		this.orderStatusService = applicationContext.getBean("orderStatusService", OrderStatusService.class);
		this.orderServ = applicationContext.getBean("orderService", OrderService.class);
		this.bollingerBandsRepo = bollingerBandsRepo;
		this.bollingerBandsEnt = bollingerBandsEnt;
	}
	
	@Override
	public void run() {
		
		System.out.println(stratID + "is running");////
		
		while(!needtoExit()) {
			String response = feedRestClient.getPreviousPeriodAndCurrentData(stockID, averagePeriod);
			String pattern = "(?<=Price\":).*?(?=,)";
			ArrayList<Double> listOfPastPrice = new ArrayList<>();
			Pattern r = Pattern.compile(pattern);
			Matcher matcher = r.matcher(response);
			while (matcher.find()) {
				listOfPastPrice.add(Double.parseDouble((matcher.group(0))));
			}
			
			int length = listOfPastPrice.size();
			currentPrice = listOfPastPrice.remove(length - 1);
			OptionalDouble optionalPeriodAverage = listOfPastPrice.stream()
					.mapToDouble(a -> a)
					.average();
			double periodAverage = optionalPeriodAverage.isPresent()? optionalPeriodAverage.getAsDouble() : 0.0; 
			double sd = getSd(periodAverage, listOfPastPrice);
			
			if (currentPriceHitsLowBand(periodAverage, sd) && !longOverMaxSize()) {
				sendOrder("buy", sizePerTrade, currentPrice);
			} else if (currentPriceHitsHighBand(periodAverage, sd) && !shortOverMaxSize()) {
				sendOrder("sell",sizePerTrade, currentPrice);
			}
			
			try {
				TimeUnit.SECONDS.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}//end while
		try {
			System.out.println("Exit while loop in " + stratID);
			isActive = false;
			bollingerBandsRepo.changeActiveStatus(bollingerBandsEnt, false);
			bollingerBandsRepo.changeSleepStatus(bollingerBandsEnt, true);
			TimeUnit.SECONDS.sleep(20);
			orderList.clear();
			updatePnlAndRoi();
			bollingerBandsRepo.setEndTime(bollingerBandsEnt, new Date().getTime());
			bollingerBandsRepo.changeSleepStatus(bollingerBandsEnt, false);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private double getSd(double average, ArrayList<Double> list) {
		if (list.isEmpty()) return 0.0;
		double temp = 0;
		for (double d: list) {
			double squrDiffToMean = Math.pow(d - average, 2);
			temp += squrDiffToMean;
		}
		double meanOfDiffs = temp / (double) list.size();
		return Math.sqrt(meanOfDiffs);
	}

	private boolean currentPriceHitsLowBand(double average, double sd) {
		return currentPrice <= average - sd * multipleOfSd;
	}
	
	private boolean currentPriceHitsHighBand(double average, double sd) {
		return currentPrice >= average + sd * multipleOfSd;
	}
	
	@Override
	public void sendOrder(String option, int size, double price) {
		super.sendOrder(option, size, price);
		updatePnlAndRoi();
	}
	
	@Override
	public void receiveReply(String corrID, String status, String option, double price, int volume) {
		super.receiveReply(corrID, status, option, price, volume);
		updatePnlAndRoi();
	}
	
	@Override
	public void prepareToRestart () {
		super.prepareToRestart();
		bollingerBandsRepo.setStartTime(bollingerBandsEnt, new Date().getTime());
		bollingerBandsRepo.setEndTime(bollingerBandsEnt, 0);
		bollingerBandsRepo.changeActiveStatus(bollingerBandsEnt, true);
		bollingerBandsRepo.updatePnl(bollingerBandsEnt, 0);
		bollingerBandsRepo.updateRoI(bollingerBandsEnt, 0);
	}
	
	private void updatePnlAndRoi() {
		bollingerBandsRepo.updatePnl(bollingerBandsEnt,(realisedShortAmount-realisedLongAmount));
		if(realisedLongAmount!=0) {
			bollingerBandsRepo.updateRoI(bollingerBandsEnt,((realisedShortAmount-realisedLongAmount)/realisedLongAmount));
		} else {
			bollingerBandsRepo.updateRoI(bollingerBandsEnt,0);
		}
	}
}
