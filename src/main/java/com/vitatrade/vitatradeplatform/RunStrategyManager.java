package com.vitatrade.vitatradeplatform;

import java.io.Console;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;



@Component
public class RunStrategyManager implements ApplicationContextAware{
	private int stratInstanceId = 0;
	private ApplicationContext applicationContext;
	
	@Autowired
	private TwoMovingAveragesRepository twoMovingRepo;
	
	@Autowired
	private PriceBreakoutRepository priceBreakRepo;
	
	@Autowired
	private BollingerBandsRepository bollingerBandsRepo;
	
	@Autowired
	private StockRepository stockRepo;
	
	Map<Integer, RunnableStrategy> activeStrategies = new HashMap<Integer, RunnableStrategy>();
	
	
	public void startStrategy(JSONObject dataObj) throws Exception {
		String startType="";
		 startType = dataObj.getString("strategyDescription");  
		 
		 switch(startType) {
		 case "Moving Average":
			String stockCode=dataObj.getString("stockCode");
			int shortAvg=dataObj.getInt("shortAverage");
			int longAvg=dataObj.getInt("longAverage");
			int crossTime=dataObj.getInt("crossoverTime");
			int stockAmt=dataObj.getInt("stockAmount");
			int stockLim=dataObj.getInt("stockLimit");

			TwoMovingAveragesEntity entInst=new TwoMovingAveragesEntity(stockRepo.getStockByCode(stockCode),shortAvg,longAvg,crossTime,stratInstanceId,stockAmt,stockLim); 
			TwoMovingAveragesStrategy newInst = new TwoMovingAveragesStrategy(stratInstanceId, stockCode, 
					stockAmt, stockLim, longAvg, shortAvg, crossTime, applicationContext,twoMovingRepo,entInst); 
			activeStrategies.put(stratInstanceId, newInst);		
			twoMovingRepo.addStrategy(entInst);
			new Thread(newInst).start();
			break;
			
		 case "Price Breakout":
			 String stockCodepb=dataObj.getString("stockCode");
			 int periodTime=dataObj.getInt("periodTime");
			 int breakoutNumber=dataObj.getInt("breakoutNumber");
			 int stockAmtpb=dataObj.getInt("stockAmount");
			 int stockLimpb=dataObj.getInt("stockLimit");			 
			 PriceBreakoutEntity pbEntInst=new PriceBreakoutEntity(stockRepo.getStockByCode(stockCodepb),periodTime ,breakoutNumber,stratInstanceId, stockAmtpb, stockLimpb);			
			 PriceBreakoutStrategy pbNewInst= new PriceBreakoutStrategy(stratInstanceId,stockCodepb,stockAmtpb,stockLimpb,periodTime,breakoutNumber,applicationContext,priceBreakRepo,pbEntInst);	 
			 activeStrategies.put(stratInstanceId, pbNewInst);
			 priceBreakRepo.addStrategy(pbEntInst);
			 new Thread(pbNewInst).start();	 
			 break;
			 
		 case "Bollinger Bands":
			 String stockCodebb=dataObj.getString("stockCode");
			 int averagePeriod = dataObj.getInt("averagePeriod");
			 int multipleOfSd = dataObj.getInt("multipleOfSd");
			 int stockAmtbb=dataObj.getInt("stockAmount");
			 int stockLimbb=dataObj.getInt("stockLimit");
			 BollingerBandsEntity bbEntInst = new BollingerBandsEntity(stockRepo.getStockByCode(stockCodebb), averagePeriod, multipleOfSd, 
					 stratInstanceId, stockAmtbb, stockLimbb);
			 BollingerBandsStrategy bbNewInst = new BollingerBandsStrategy(stratInstanceId, stockCodebb, stockAmtbb, stockLimbb, averagePeriod, 
					 multipleOfSd, applicationContext, bollingerBandsRepo, bbEntInst);
			 activeStrategies.put(stratInstanceId, bbNewInst);
			 bollingerBandsRepo.addStrategy(bbEntInst);
			 new Thread(bbNewInst).start();
			 
	     default:
	    	System.out.println("Invalid strategy");
		
		}
		stratInstanceId++;
	}

	
	public void restartStrategy(int stratID) {
		System.out.println("restartgin strategy"+stratID);
		RunnableStrategy strategyToRestart = activeStrategies.get(stratID);
		if (strategyToRestart.isActive) return;
		strategyToRestart.prepareToRestart();
		new Thread(strategyToRestart).start();
	}
	
	public void stopStrategy(int stratID) {
		System.out.println("stopping strategy"+stratID);
		activeStrategies.get(stratID).forceStop();
	}
	
	public void replyReceiver(String message, String corrID) {
		System.out.println("Receiving from strManager: " + message);///////
		JSONObject response =new JSONObject(message);
		int instID=(int) response.get("straId");		
		RunnableStrategy stratInst = activeStrategies.get(instID);
		if (stratInst == null) return;
		String option=((boolean)response.get("buy")==true)?"buy":"sell";
		stratInst.receiveReply(corrID, response.getString("result"),option, response.getDouble("price"), response.getInt("size"));
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext; 
	}

}
