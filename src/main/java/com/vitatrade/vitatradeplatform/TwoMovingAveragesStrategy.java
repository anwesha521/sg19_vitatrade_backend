package com.vitatrade.vitatradeplatform;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.springframework.context.ApplicationContext;

import com.vitatrade.brokerservice.OrderSender;



public class TwoMovingAveragesStrategy extends RunnableStrategy {
	private int longPeriod;
	private int shortPeriod;
	private int crossTime;
	private TwoMovingAveragesRepository twoMoveRepo;
	private TwoMovingAveragesEntity twoMoveEnt;
	
	public TwoMovingAveragesStrategy(int stratID, String stockID, int sizePerTrade, int maximumSize,  
			int longPeriod, int shortPeriod, int crossTime, ApplicationContext applicationContext,TwoMovingAveragesRepository twoMoveRepo,TwoMovingAveragesEntity twoMoveEnt) {
		this.stratID = stratID;
		this.stockID = stockID;
		this.sizePerTrade = sizePerTrade;
		this.maximumSize = maximumSize;
		this.longPeriod= longPeriod;
		this.shortPeriod = shortPeriod;
		this.crossTime = crossTime;
		this.applicationContext = applicationContext;
		this.feedRestClient = applicationContext.getBean("feedRestClient", FeedRestClient.class);
		this.orderSender = applicationContext.getBean("orderSender", OrderSender.class);
		this.orderStatusService = applicationContext.getBean("orderStatusService", OrderStatusService.class);
		this.orderServ = applicationContext.getBean("orderService", OrderService.class);
		this.twoMoveRepo=twoMoveRepo;
		this.twoMoveEnt=twoMoveEnt;
	}
	
	@Override
	public void run(){
		
		System.out.println(stratID + "is running");
		
		boolean wasBelow=false;
		boolean firstTime=true;
		while(!needtoExit()) {
			String feedShort = feedRestClient.getFeedPrice(stockID, shortPeriod);
			String feedLong = feedRestClient.getFeedPrice(stockID, longPeriod);
			double shortAvg =getAvg(feedShort);
			double longAvg =getAvg(feedLong);
			
			double price=getLastPrice(feedShort);
			currentPrice=price;
			System.out.println("price="+price);
			if(firstTime) {
				firstTime=false;
				wasBelow=longAvg<shortAvg;
				continue;
			}
			if((longAvg>=shortAvg)&&wasBelow) {
				if(!shortOverMaxSize()) {
					sendOrder("sell",sizePerTrade, price);
					wasBelow=false;
				}
			}
			
			if(longAvg<shortAvg && !wasBelow) {
				if(!longOverMaxSize()) {
					sendOrder("buy",sizePerTrade, price);
					wasBelow = true;
				}
			}
			
			try {
				TimeUnit.SECONDS.sleep(crossTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}// close while loop
		try {
			System.out.println("Exit while loop in " + stratID);
			isActive = false;
			twoMoveRepo.changeActiveStatus(twoMoveEnt, false);
			twoMoveRepo.changeSleepStatus(twoMoveEnt, true);
			TimeUnit.SECONDS.sleep(20);
			orderList.clear();
			updatePnlAndRoi();
			twoMoveRepo.setEndTime(twoMoveEnt, new Date().getTime());
			twoMoveRepo.changeSleepStatus(twoMoveEnt, false);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public double getAvg(String feed) {
		String pattern = "(?<=Price\":).*?(?=,)";
		Pattern r = Pattern.compile(pattern); 
		int count=0;
		double sum=0;
		java.util.regex.Matcher m = r.matcher(feed);
		       while (m.find( )) {
		    	   count++;
		    	   sum+=Double.parseDouble(m.group(0));
		    	   
		       }
		 
		return (sum/count);	
	}
	
	public double getLastPrice(String feed) {
		String pattern = "(?<=Price\":).*?(?=,)";
		Pattern r = Pattern.compile(pattern); 			
		java.util.regex.Matcher m = r.matcher(feed);
		
		double lastPrice=100.0;
		       while (m.find( )) {
		    	   lastPrice=Double.parseDouble(m.group(0));
		       }
		return lastPrice;
	}
	
	@Override
	public void forceStop() {
		isActive = false;
	}
	
	@Override
	public void sendOrder(String option, int size, double price) {
		super.sendOrder(option, size, price);
		updatePnlAndRoi();
	}

	@Override
	public void receiveReply(String corrID, String status, String option, double price,int volume) {
		super.receiveReply(corrID, status, option, price, volume);
		updatePnlAndRoi();
	}
	
	@Override
	public void prepareToRestart () {
		super.prepareToRestart();
		twoMoveRepo.setStartTime(twoMoveEnt, new Date().getTime());
		twoMoveRepo.setEndTime(twoMoveEnt, 0);
		twoMoveRepo.changeActiveStatus(twoMoveEnt, true);
		twoMoveRepo.updatePnl(twoMoveEnt, 0);
		twoMoveRepo.updateRoI(twoMoveEnt, 0);
	}
	
	private void updatePnlAndRoi() {
		twoMoveRepo.updatePnl(twoMoveEnt,(realisedShortAmount-realisedLongAmount));
		if(realisedLongAmount!=0) {
			twoMoveRepo.updateRoI(twoMoveEnt,((realisedShortAmount-realisedLongAmount)/realisedLongAmount));
		} else {
			twoMoveRepo.updateRoI(twoMoveEnt,0);
		}
	}
}
