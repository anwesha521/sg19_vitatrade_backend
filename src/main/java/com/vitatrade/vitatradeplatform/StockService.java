package com.vitatrade.vitatradeplatform;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StockService {

	@Autowired
	private StockRepository stockRepo;
	
	//@Autowired
	private RestTemplate template = new RestTemplate();
	
	public void createNewstock(String stockCode, String stockName) {
		Stock s  = new Stock(stockCode, stockName);
		stockRepo.insertStock(s);
	}
	
	public void deleteStockByStockCode(String stockCode) {
		Stock s =  stockRepo.getStockByCode(stockCode);
		stockRepo.deleteStock(s);
	}
	
	public Stock getStockByCode(String stockCode) {
		return stockRepo.getStockByCode(stockCode);
	}
	
	//to get all the stocks in database
	public List<Stock> getAllStocks(){
	    return stockRepo.getAllStocks();
	}
	
	//to populate the stock table in database
	public void fetchStocksFromFeed() {
		String url = "http://feed2.conygre.com/API/StockFeed/GetSymbolList";
        Company[] companies = template.getForObject(url, Company[].class);
        for(Company c: companies) {
			createNewstock(c.getSymbol(), c.getName());
		}
	}
}
