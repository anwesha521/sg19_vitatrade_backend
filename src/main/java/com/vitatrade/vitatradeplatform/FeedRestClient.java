package com.vitatrade.vitatradeplatform;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class FeedRestClient {
	
	private final String basicFeedBaseAddress = "http://feed.conygre.com:8080/MockYahoo/quotes.csv";
	private final String enhancedFeedBaseAddress = "http://feed2.conygre.com/API/StockFeed/";
	private final String liveFeed="http://feed.conygre.com:8080/MockYahoo/quotes.csv?s=";
	private RestTemplate template = new RestTemplate();

	public String getFeedPrice(String stock, int duration){
		String response = template.getForObject(enhancedFeedBaseAddress + "GetStockPricesForSymbol/" + 
				stock + "?HowManyValues=" + duration, String.class);
		return response;
	}
	
	public String getPriceforGraph(String stock){
		String response = template.getForObject(liveFeed + stock + 
				"&f=p0", String.class);
		return response;
	}
	
	public String getPreviousPeriodAndCurrentData(String stock, int window) {
		String response = template.getForObject(enhancedFeedBaseAddress + "GetStockPricesForSymbol/" +
				stock + "?HowManyValues=" + (window + 1), String.class);
		return response;
	}

}
