package com.vitatrade.vitatradeplatform;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Company {
	
	@JsonProperty("CompanyName")
	private String name;
	
	@JsonProperty("Symbol")
	private String symbol;
	
	@JsonProperty("SymbolID")
	private int id;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getId() {
		return id;
	}
	
	
	
}  