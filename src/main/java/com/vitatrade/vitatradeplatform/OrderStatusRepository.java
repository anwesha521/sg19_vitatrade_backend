package com.vitatrade.vitatradeplatform;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

@Repository
public class OrderStatusRepository {

	@PersistenceContext(name="myPersistenceUnit")
	protected EntityManager entityManager;
	
	final private int FILLED_ID = 2;
	final private int PARTIALLY_FILLED_ID = 3;
	final private int REJECTED_ID = 4;
	
	public OrderStatus getOrderStatusByDescription(String status) {
		int id = -1;
		switch(status) {
			case "FILLED":
				id = FILLED_ID;
				break;
			case "PARTIALLY_FILLED":
				id = PARTIALLY_FILLED_ID;
				break;
			case "REJECTED":
				id = REJECTED_ID;
				break;
		}
		if(id != -1) {
			return entityManager.find(OrderStatus.class, id);
		}else
			return null;
	}
	
	//get initial OrderStatus (i.e. pending)
	public OrderStatus getPendingStatus() {
		String jpql = "select os from OrderStatus os where os.statusId = 1";
		TypedQuery<OrderStatus> query = entityManager.createQuery(jpql, OrderStatus.class);
		return query.getSingleResult();
	}
}
