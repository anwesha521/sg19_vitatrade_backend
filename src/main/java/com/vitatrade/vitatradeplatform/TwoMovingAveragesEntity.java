package com.vitatrade.vitatradeplatform;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="STRATEGY_TWOMOVINGAVG", schema="VITATRADESCHEMA")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class TwoMovingAveragesEntity {
	
		@Id
		private int strategyId;
		
		@ManyToOne
		@JoinColumn(name = "STOCK_CODE")
		private Stock stock;
		
		private int longAvgDuration;
		private int shortAvgDuration;
		private int crossPeriod;
		private int size;
		private int maxAmount;
		private double overallPnl;
		private double overallRoi;
		private boolean isActive;
		private Timestamp startTime;
		private Timestamp exitTime;
		private boolean isSleeping;
		
	

		public TwoMovingAveragesEntity() {}
		
		public TwoMovingAveragesEntity(Stock stock, int shortp,int longp, int crossTime, int stratid,int vol, int max) {
			this.stock=stock;	
			this.shortAvgDuration=shortp;
			this.longAvgDuration=longp;
			this.crossPeriod=crossTime;
			
			this.strategyId=stratid;
			this.size=vol;
			this.maxAmount=max;
		
			this.isActive=true;
			this.isSleeping=false;
			this.startTime=new Timestamp(new Date().getTime());
			this.exitTime=null;
			this.overallPnl=0;
			this.overallRoi=0;
			
		}
		public int getStratID() {
			return strategyId;
		}

		public void setStratID(int stratID) {
			this.strategyId = stratID;
		}

		public Stock getStockID() {
			return stock;
		}

		public void setStockID(Stock stockID) {
			this.stock = stockID;
		}
		public boolean isSleepStatus() {
			return isSleeping;
		}

		public void setSleepStatus(boolean sleepStatus) {
			this.isSleeping = sleepStatus;
		}
		public int getLongPeriod() {
			return longAvgDuration;
		}

		public void setLongPeriod(int longPeriod) {
			this.longAvgDuration = longPeriod;
		}

		public int getShortPeriod() {
			return shortAvgDuration;
		}

		public void setShortPeriod(int shortPeriod) {
			this.shortAvgDuration = shortPeriod;
		}

		public int getCrossTime() {
			return crossPeriod;
		}

		public void setCrossTime(int crossTime) {
			this.crossPeriod = crossTime;
		}

		public int getVolume() {
			return size;
		}

		public void setVolume(int volume) {
			this.size = volume;
		}

		public int getMaxamt() {
			return maxAmount;
		}

		public void setMaxamt(int maxamt) {
			this.maxAmount = maxamt;
		}

		public double getPnl() {
			return overallPnl;
		}

		public void setPnl(double pnl) {
			this.overallPnl = pnl;
		}

		public double getRoi() {
			return overallRoi;
		}

		public void setRoi(double roi) {
			this.overallRoi = roi;
		}

//		public int getTotalLong() {
//			return totalLong;
//		}
//
//		public void setTotalLong(int totalLong) {
//			this.totalLong = totalLong;
//		}
//
//		public int getTotalShort() {
//			return totalShort;
//		}
//
//		public void setTotalShort(int totalShort) {
//			this.totalShort = totalShort;
//		}

		public boolean isActive() {
			return isActive;
		}

		public void setActive(boolean isActive) {
			this.isActive = isActive;
		}

		public Timestamp getStartTime() {
			return startTime;
		}

		public void setStartTime(long startTime) {
			this.startTime = new Timestamp(startTime);
		}

		public Timestamp getEndTime() {
			return exitTime;
		}

		public void setEndTime(long endTime) {
			if (endTime == 0) {
				this.exitTime = null;
			} else {
				this.exitTime = new Timestamp(endTime);
			}
		}
		@Override
		public String toString() {
			return (this.crossPeriod+"\t"+this.exitTime+"\t"+this.isActive);
		}

		

}
