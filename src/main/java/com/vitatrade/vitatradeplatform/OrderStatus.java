package com.vitatrade.vitatradeplatform;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ORDERSTATUS")
public class OrderStatus {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int statusId;

	private String statusDescription;
	
	public OrderStatus() {
		
	}
	
	public OrderStatus(int statusId, String description) {
		this.statusId = statusId;
		this.statusDescription = description;
	}
	
	public int getStatusId() {
		return this.statusId;
	}

	public String getStatusDescription() {
		return this.statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
}
