package com.vitatrade.vitatradeplatform;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vitatrade.brokerservice.OrderSender;

public class PriceBreakoutStrategy extends RunnableStrategy {
	private int window;
	private int threshold;
	private PriceBreakoutRepository priceBreakRepo;
	private PriceBreakoutEntity pbEnt;

	public PriceBreakoutStrategy(int stratID, String stockID, int sizePerTrade, int maximumSize,  
			int window, int threshold, ApplicationContext applicationContext,PriceBreakoutRepository priceBreakRepo,PriceBreakoutEntity pbEnt) {
		this.stratID = stratID;
		this.stockID = stockID;
		this.sizePerTrade = sizePerTrade;
		this.maximumSize = maximumSize;
		this.window = window;
		this.threshold = threshold;
		this.applicationContext = applicationContext;
		this.feedRestClient = applicationContext.getBean("feedRestClient", FeedRestClient.class);
		this.orderSender = applicationContext.getBean("orderSender", OrderSender.class);
		this.orderStatusService = applicationContext.getBean("orderStatusService", OrderStatusService.class);
		this.orderServ = applicationContext.getBean("orderService", OrderService.class);
		this.priceBreakRepo=priceBreakRepo;
		this.pbEnt=pbEnt;
	}
	
	@Override
	public void run() {
		int hitCount = 0;
		while (!needtoExit()) {
			if (window < 1) break;
			String response = feedRestClient.getPreviousPeriodAndCurrentData(stockID, window);
			String pattern = "(?<=Price\":).*?(?=,)";
			ArrayList<Double> listOfPastPrice = new ArrayList<>();
			Pattern r = Pattern.compile(pattern);
			Matcher matcher = r.matcher(response);
			while (matcher.find()) {
				listOfPastPrice.add(Double.parseDouble((matcher.group(0))));
			}
			
			int length = listOfPastPrice.size();
			currentPrice = listOfPastPrice.get(length - 1);
			double previousOpen = listOfPastPrice.get(0);
			double previousClose = listOfPastPrice.get(length - 2);
			double currentHigh = listOfPastPrice.get(1);
			for (int i = 2; i < length; i++) {
				if (listOfPastPrice.get(i) > currentHigh) {
					currentHigh = listOfPastPrice.get(i);
				}
			}
			
			double currentLow = listOfPastPrice.get(1);
			for (int i = 2; i < length; i++) {
				if (listOfPastPrice.get(i) < currentLow) {
					currentLow = listOfPastPrice.get(i);
				}
			}
			
			
			
//			if ((currentHigh <= previousOpen && currentLow >= previousClose) || 
//					(currentHigh <= previousClose && currentLow >= previousOpen)) {
//				hitCount++;
//			} 
			
			if (Math.abs(currentHigh - currentLow) <= Math.abs(previousOpen - previousClose)) {
				hitCount++;
			}
			
			else {
				if (hitCount >= threshold) {
					if ((currentHigh <= previousOpen && currentLow < previousClose)|| 
							(currentHigh <= previousClose && currentLow < previousOpen)||
							(currentHigh > previousOpen && currentLow >= previousClose)|| 
							(currentHigh > previousClose && currentLow >= previousOpen)) {
						double currentOpen = listOfPastPrice.get(1);
						if (currentPrice > currentOpen && !longOverMaxSize()) {
							sendOrder("buy", sizePerTrade, currentPrice);
							
						} else if (currentPrice < currentOpen && !shortOverMaxSize()) {
							sendOrder("sell", sizePerTrade, currentPrice);
						}else {
							hitCount++;
						}
					} else {
						hitCount = 0;
					}
				} else {
					hitCount = 0;
				}				
			}
			
			
			try {
				TimeUnit.SECONDS.sleep(5);
				System.out.println("Hello: " + hitCount);//////
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}// close while
		try {
			System.out.println("Exit while loop in " + stratID);
			isActive = false;
			priceBreakRepo.changeActiveStatus(pbEnt, false);
			priceBreakRepo.changeSleepStatus(pbEnt, true);
			TimeUnit.SECONDS.sleep(20);
			orderList.clear();
			updatePnlAndRoi();
			priceBreakRepo.setEndTime(pbEnt, new Date().getTime());
			priceBreakRepo.changeSleepStatus(pbEnt, false);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void sendOrder(String option, int size, double price) {
		super.sendOrder(option, size, price);
		updatePnlAndRoi();
	}
	
	@Override
	public void receiveReply(String corrID, String status, String option, double price, int volume) {
		super.receiveReply(corrID, status, option, price, volume);
		updatePnlAndRoi();
	}
	
	@Override
	public void prepareToRestart () {
		super.prepareToRestart();
		priceBreakRepo.setStartTime(pbEnt, new Date().getTime());
		priceBreakRepo.setEndTime(pbEnt, 0);
		priceBreakRepo.changeActiveStatus(pbEnt, true);
		priceBreakRepo.updatePnl(pbEnt, 0);
		priceBreakRepo.updateRoI(pbEnt, 0);
	}
	
	private void updatePnlAndRoi() {
		priceBreakRepo.updatePnl(pbEnt,(realisedShortAmount-realisedLongAmount));
		if(realisedLongAmount!=0) {
			priceBreakRepo.updateRoI(pbEnt,((realisedShortAmount-realisedLongAmount)/realisedLongAmount));
		} else {
			priceBreakRepo.updateRoI(pbEnt,0);
		}
	}
}
