package com.vitatrade.brokerservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.vitatrade.vitatradeplatform.RunStrategyManager;

@Component
@ComponentScan(basePackageClasses = {com.vitatrade.vitatradeplatform.RunStrategyManager.class})
public class ResponseReceiver {

	@Autowired
	RunStrategyManager manager;
	
	@JmsListener(destination="OrderBroker_Reply")
	public void receiveResponseMessage(@Payload String message,
			@Header(JmsHeaders.CORRELATION_ID) String incomingCorrId) {
		// to do: process the json message
		System.out.println("----------Response Receiver: receive order response----------");
		System.out.println("testing receiving response" + message + "with corr id: " + incomingCorrId);
		message = message.replace("[", "");
		message = message.replace("]", "");
		System.out.println("after process" + message);
		//{"result":"FILLED","size":100,"orderId":[38],"price":[38.835],"buy":[false],"stock":["a"],"whenAsDate":["2019-08-23 15:31:39.123"]}
		manager.replyReceiver(message, incomingCorrId);
	}
	
}
