package com.vitatrade.brokerservice;

import java.util.Random;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class OrderReceiver {
	
	@Autowired
	ResponseSender sender;
	
	int filled_percentage = 80;
	int partially_filled_percentage = 10;
	int rejected_percentage = 10;
	
	Logger log = Logger.getLogger(OrderReceiver.class.getName());
	
	@JmsListener(destination="OrderBroker")
	public void receiveOrderMessage(@Payload String message,
									@Header(JmsHeaders.CORRELATION_ID) String incomingCorrId) {
		
		if(incomingCorrId == null) {
			log.warning("JMS Correlation ID Not Found: no response will be sent");
			return;
		}
		System.out.println("----------Order Receiver: receive order message----------");
		log.info(message + " (correlation id: " + incomingCorrId + ")");
		JSONObject order = new JSONObject(message);
		JSONArray sizeArr = (JSONArray) order.get("size");
		int requestSize = sizeArr.getInt(0);
		
		String result = "";
		int confirmedSize = 0;
		Random random = new Random();
		int randomNumber = random.nextInt(100);
		
		//FILLED -- 80%, PARTIALLY_FILLED -- 10%, REJECTED -- 10%
		if(randomNumber>=0 && randomNumber<filled_percentage) {
			result = "FILLED";
			confirmedSize = requestSize;
		}else if(randomNumber>=filled_percentage && randomNumber<filled_percentage+partially_filled_percentage) {
			result = "PARTIALLY_FILLED";
			confirmedSize = generatePartiallyFilledSize(requestSize);
		}else {
			result = "REJECTED";
			confirmedSize = 0;
		}
		
		order = order.put("result", result);
		order = order.put("size", confirmedSize);
		log.info("Sending response: Corr ID - " + incomingCorrId + " Status - " + result);
		sender.sendResponseToStrategy(order.toString(), incomingCorrId); 
	}
	
	public int generatePartiallyFilledSize(int requestSize) {
		
		Random random = new Random();
		double percentage = random.nextDouble();
		int confirmedSize = (int) (requestSize*percentage);
		return confirmedSize;
	}
}
