package com.vitatrade.brokerservice;

import java.util.UUID;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.vitatrade.vitatradeplatform.Order;

@Component
public class OrderSender {

	@Autowired
	private JmsTemplate jmsTemplate;
	
	public String sendOrderToBroker(Order orderToSend) {
	    
		String orderMsgToSend = generateOrderMsg(orderToSend);
		
		String correlationalID = UUID.randomUUID().toString();
		System.out.println("----------OrderSender: ready to send order----------");
        System.out.println(orderMsgToSend + "with corr id: " + correlationalID);
        jmsTemplate.convertAndSend("OrderBroker", orderMsgToSend, m -> {
            //log.info("setting standard JMS headers before sending");
            m.setJMSCorrelationID(correlationalID);
            return m;
        });	
        
        try {
        	Thread.sleep(100); 
        } 
        catch (Exception ex) {}
        
        return correlationalID;
    }
	
	public String generateOrderMsg(Order order) {
		
		JSONObject orderJson = new JSONObject();
		orderJson.append("buy", order.isBuy());
		orderJson.append("orderId", order.getOrderId());
		orderJson.append("price", order.getPrice());
		orderJson.append("size", order.getInitial_size());
		orderJson.append("stock", order.getStock().getStockCode());
		orderJson.append("whenAsDate", order.getOrder_time());
		orderJson.append("straId", order.getStrategyId());
		
		return orderJson.toString();
	}
}
