package com.vitatrade.brokerservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class ResponseSender {

	@Autowired
	private JmsTemplate jmsTemplate;

	public void sendResponseToStrategy(String message, String correlationID) {
    
		String messageToSend = message;
		System.out.println("----------Response Sender: ready to send response----------");
        System.out.println(messageToSend + "with corr id: " + correlationID);
        jmsTemplate.convertAndSend("OrderBroker_Reply", messageToSend, m -> {
            //log.info("setting standard JMS headers before sending");
            m.setJMSCorrelationID(correlationID);
            return m;
        });	
        
        try {
        	Thread.sleep(300); 
        } 
        catch (Exception ex) {}
    }

}
