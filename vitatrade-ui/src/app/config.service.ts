import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ConfigService {
  constructor(private http: HttpClient) { }
  
 
  getConfig(selectedOption: string) {
    var configUrl='/livefeed?stock='+selectedOption+'&f=p0';
    return this.http.get(configUrl);
  }
}