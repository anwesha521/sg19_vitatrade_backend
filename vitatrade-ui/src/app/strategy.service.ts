import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { tap, map } from 'rxjs/operators';
import { throwError, forkJoin } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class StrategyService {

  //private baseUrl = 'http://localhost:9090';

  constructor(private http: HttpClient) { }

  startStrategy(strategy: String): void { //CAN ONLY PASS STRING OBJECTS COME IN AS EMPTY OBJECTS (objects come in as empty may be due to headers)
    this.http.post('/start', strategy)
      .subscribe(
        data => {
          console.log("POST Request is successful ", data);
        },
        error => {
          console.log("Error", error);
        }
      );

  }

  stopStrategy(strategy: String): void { //CAN ONLY PASS STRING OBJECTS COME IN AS EMPTY OBJECTS (objects come in as empty may be due to headers)
    this.http.post('/stop', strategy)
      .subscribe(
        data => {
          console.log("POST to stop strat ID ", data);
        },
        error => {
          console.log("Error", error);
        }
      );

  }

  restartStrategy(strategy: String): void { //CAN ONLY PASS STRING OBJECTS COME IN AS EMPTY OBJECTS (objects come in as empty may be due to headers)
    this.http.post('/restart', strategy)
      .subscribe(
        data => {
          console.log("POST to restart strategy ", data);
        },
        error => {
          console.log("Error", error);
        }
      );

  }

  getMovingAvgStrategies(): Observable<any> {
    return this.http.get('/movingAverageStrats')
      .pipe(map(this.extractMovingAvgData));
  }

  private extractMovingAvgData(res: Response) {
    console.log('MA BEFORE PROCESSING' + res);
    for (let entry in res) {
      res[entry]["strategy"] = "Moving Average";
      if (res[entry].active && !res[entry].sleepStatus) { //running will be active and not sleeping
        res[entry]["status"] = "Running";
      } else if (!res[entry].active && !res[entry].sleepStatus){ // not active & not sleeping = inactive
        res[entry]["status"] = "Exited";
      } else {
        res[entry]["status"] = "Exiting..."
      }
      res[entry]["PnL"] = res[entry].pnl.toFixed(2);
      res[entry]["ROI"] = (res[entry].roi * 100).toFixed(2);       
    }
    console.log('MA AFTER PROCESSING' + res);
    return res || {}; //return empty object if no response
  }

  private extractPriceBreakoutData(res: Response) {
    console.log(res);
    for (let entry in res) {
      res[entry]["strategy"] = "Price Breakout";
      if (res[entry].active && !res[entry].sleepStatus) { //running will be active and not sleeping
        res[entry]["status"] = "Running";
      } else if (!res[entry].active && !res[entry].sleepStatus){ // not active & not sleeping = inactive
        res[entry]["status"] = "Exited";
      } else {
        res[entry]["status"] = "Exiting..."
      }
      res[entry]["PnL"] = res[entry].pnl.toFixed(2);
      res[entry]["ROI"] = (res[entry].roi * 100).toFixed(2);      
    }
    return res || {}; //return empty object if no response
  }

  private extractBollingerBandsData(res: Response) {
    console.log('MA BEFORE PROCESSING' + res);
    for (let entry in res) {
      res[entry]["strategy"] = "Bollinger Bands";
      if (res[entry].active && !res[entry].sleepStatus) { //running will be active and not sleeping
        res[entry]["status"] = "Running";
      } else if (!res[entry].active && !res[entry].sleepStatus){ // not active & not sleeping = inactive
        res[entry]["status"] = "Exited";
      } else {
        res[entry]["status"] = "Exiting..."
      }
      res[entry]["PnL"] = res[entry].pnl.toFixed(2);
      res[entry]["ROI"] = (res[entry].roi * 100).toFixed(2);       
    }
    console.log('MA AFTER PROCESSING' + res);
    return res || {}; //return empty object if no response
  }

  getPriceBreakoutStrategies(): Observable<any> {
    return this.http.get('/priceBreakoutStrats')
      .pipe(map(this.extractPriceBreakoutData));
  }

  getBollingerBandsStrategies(): Observable<any> {
    return this.http.get('/bollingerBandsStrats')
      .pipe(map(this.extractBollingerBandsData));
  }

  getMultipleStrategies(): Observable<any[]> {
    let response1 = this.getMovingAvgStrategies();
    let response2 = this.getPriceBreakoutStrategies();
    let response3 = this.getBollingerBandsStrategies();
    return forkJoin([response1, response2, response3]); // cannot use map on forkJoin
  }

 
}

