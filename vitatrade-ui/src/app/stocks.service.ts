import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class StocksService {    

  //private baseUrl = 'http://localhost:9090/'

  constructor(private http: HttpClient) { }
  
  getStocks(): Observable<any> {
    return this.http.get('/stocks')
                    .pipe(map(this.extractData));
                                      
  }

  private extractData(res:Response){
    return res || {};
  }
}