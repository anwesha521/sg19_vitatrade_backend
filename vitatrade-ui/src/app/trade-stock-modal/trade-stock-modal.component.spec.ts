import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeStockModalComponent } from './trade-stock-modal.component';

describe('TradeStockModalComponent', () => {
  let component: TradeStockModalComponent;
  let fixture: ComponentFixture<TradeStockModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradeStockModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeStockModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
