import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Stock } from '../stocks';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TradeStockModalComponent } from '../trade-stock-modal/trade-stock-modal.component';
import { Observable } from 'rxjs';
import { StocksService } from '../stocks.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

// DEBUG
/*
const stockList: Stock[]=[
  {stockCode: "GOOG", stockName: "Google"},
  {stockCode: "AAPL", stockName: "Apple"},
  {stockCode: "NSC", stockName: "New Stock Company"},
  {stockCode: "BRK-A", stockName: "srathsb ey"},
  {stockCode: "SEF", stockName: "A TSR"},
  {stockCode: "SATV", stockName: "tartv"},
  {stockCode: "BRN", stockName: "etysvb"},
  {stockCode: "IY", stockName: "asvtramr"},
  {stockCode: "YWTE", stockName: "agilent"},
  {stockCode: "VZCX", stockName: "lazy"}
]; 
*/

@Component({
  selector: 'app-trade-stocks',
  templateUrl: './trade-stocks.component.html',
  styleUrls: ['./trade-stocks.component.css']
})
 
export class TradeStocksComponent implements OnInit {

  displayedColumns: string[]=['stockCode', 'stockName', 'startStrategy'];
  dataSource :MatTableDataSource<Stock>;
   
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    //this.dataSource = new MatTableDataSource<Stock>(stockList); //DEBUG
    this.dataSource = new MatTableDataSource();
    this.dataSource.paginator = this.paginator;
    this.getStocks();
  }

  constructor(private modalService: NgbModal, private stockService: StocksService) { }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  tradeStock(stockSelected) {
    const modalRef = this.modalService.open(TradeStockModalComponent);
    modalRef.componentInstance.stock = stockSelected;
  } 

  getStocks(){
    this.stockService.getStocks().subscribe((data: Stock[]) => {
      // console.log(data); //DEBUG
      // console.log('S t 0 n k $'); //DEBUG
      this.dataSource.data = data; // on data receive populate dataSource.data array
      return data;
   });
  }

}