import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class OrdersService {    

  //private baseUrl = 'http://localhost:9090/orderHistory'

  constructor(private http: HttpClient) { }
  
  getOrdersHistory(): Observable<any> {  
    return this.http.get('/orderHistory')
                    .pipe(map(this.extractData));
   }

  private extractData(res:Response){
    console.log(res, typeof(res));
    /*
    for (let entry in res){
      res[entry]["test"]="testing";
    }
    */
    return res || {}; //return empty object if no response

  }
}