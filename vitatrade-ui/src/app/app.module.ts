import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ChartModule } from 'angular-highcharts';
import { MatTableModule, MatInputModule, MatFormFieldControl } from '@angular/material';
import { MatPaginatorModule,MatFormFieldModule } from '@angular/material';
import { HighchartsService } from './highcharts.service';
import { ConfigService } from './config.service';
import { FeedComponent } from './feed/feed.component';
import { FormsModule } from '@angular/forms';
import { OrdersTableComponent } from './orders-table/orders-table.component';
import { PositionsTableComponent } from './positions-table/positions-table.component';
import { TradeStocksComponent } from './trade-stocks/trade-stocks.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TradeStockModalComponent } from './trade-stock-modal/trade-stock-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StopStrategyModalComponent } from './stop-strategy-modal/stop-strategy-modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StrategyService } from './strategy.service';

@NgModule({
  declarations: [
    AppComponent,
    FeedComponent,
    OrdersTableComponent,
    PositionsTableComponent,
    TradeStocksComponent,
    TradeStockModalComponent,
    StopStrategyModalComponent
 ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AngularFontAwesomeModule,
    NgbModule,
    MatTableModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    MatInputModule    
  ],
  entryComponents: [TradeStockModalComponent,StopStrategyModalComponent],
  providers: [HighchartsService, ConfigService, StrategyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
