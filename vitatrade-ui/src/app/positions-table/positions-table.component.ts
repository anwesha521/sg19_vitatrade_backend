import { Component, OnInit, ViewChild } from '@angular/core';
import { Position } from './position';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StopStrategyModalComponent } from '../stop-strategy-modal/stop-strategy-modal.component';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { StrategyService } from '../strategy.service';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

/*
const  positionsList: Position[]=[
  {id:1, strategy: "moving average", {stockCode: "GOOG",stockDescription: "Google"},position:"Long", volume: 6000, PnL:-687.94, ROI: -0.6, status:"active"},
  {id:2, strategy: "price breakout", {stockCode: "GOOG",stockDescription: "Google"},position:"Short", volume: 10000, PnL:9452.94, ROI: 0.5, status:"exiting"},
  {id:3, strategy: "price breakout", {stockCode: "GOOG",stockDescription: "Google"},position:"Long", volume: 420, PnL:-687.94, ROI: 0, status:"active"},
  {id:3, strategy: "price breakout", {stockCode: "GOOG",stockDescription: "Google"},position:"Long", volume: 420, PnL:-687.94, ROI: 0, status:"active"},
  {id:3, strategy: "price breakout", {stockCode: "GOOG",stockDescription: "Google"},position:"Long", volume: 420, PnL:-687.94, ROI: 0, status:"active"}
];
*/

@Component({
  selector: 'app-positions-table',
  templateUrl: './positions-table.component.html',
  styleUrls: ['./positions-table.component.css']
})
export class PositionsTableComponent implements OnInit {
  // this will determine the number of columns we have
  displayedColumns: string[] = ['#', 'strategy', 'stockCode', 'volume', 'PnL', 'ROI', 'status', 'actionOnStrategy'];
  dataSource: MatTableDataSource<Position>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private modalService: NgbModal, private strategyService: StrategyService) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<Position>(); //DEBUG
    this.getAllStrategies(); //populate data source
    this.dataSource.paginator = this.paginator;
    setInterval(() => {this.getAllStrategies()           
    }, 1000);
  }


  restartStrategy(stratSelected) {
    console.log("RESTARTED STATEGY:" + stratSelected);
    this.strategyService.restartStrategy(stratSelected);
  }

  // **** HARD CODED DEBUG   ****//
  stopStrategy(stratSelected) {
    // inform trader
    const modalRef = this.modalService.open(StopStrategyModalComponent);
    modalRef.componentInstance.stock = stratSelected;
    console.log("Ending strategy #" + stratSelected);
    this.strategyService.stopStrategy(stratSelected);

    // make sure button not clickable for 30 seconds
    /*
    for(let i = 0; i < positionsList.length; ++i){
      if (positionsList[i].id === stratSelected) {
          positionsList.splice(i,1);
      }
  }
  */
  }



  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAllStrategies() {
    console.log("getting strategies");
    /*
    this.strategyService.getMovingAvgStrategies().subscribe((data: Position[]) => {
      // WHEN RELOADING MAY HAVE TO COMPARE AND UPDATE OR SOMETHING
       console.log(data)
       this.dataSource.data = data; // on data receive populate dataSource.data array
       return data;
    });
    */

    this.strategyService.getMultipleStrategies().subscribe(responseList => {
      console.log('FETCHING AND COMBINING DATA');
      //console.log(responseList[0]);
      //console.log(responseList[1]);
      this.dataSource.data = responseList[0].concat(responseList[1]).concat(responseList[2]);
    });

  }



}
