import { OrderStatus } from './order-status';

import { Stock } from '../stocks';

export interface Order {
    orderId: number;
    buy: boolean;
    price: number;
    initial_size: number;
    approvedSize: number;
    total: number;
    order_time: Date;
    stock: Stock;
    orderStatus: OrderStatus; 
    strategyId: number;  
    
    /**PROPERTY NAMES HAVE IRREGULAR FORMATTING IDK WHY CONSOLE LOG PREPERTY NAMES DONT FOLLOW THE ONES I SET */
}