import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Order } from './order';
import { OrdersService } from '../orders.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-orders-table',
  templateUrl: './orders-table.component.html',
  styleUrls: ['./orders-table.component.css']
})

export class OrdersTableComponent implements OnInit {
  displayedColumns: string[] = ['#', 'isbuy', 'price', 'initialSize', 'approvedSize', 'total', 'orderTime', 'stock', 'orderStatus', 'strategyId'];
  dataSource: MatTableDataSource<Order>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;


  constructor(private orderService: OrdersService) { }

  ngOnInit() {
    
    this.dataSource = new MatTableDataSource();
    this.dataSource.paginator = this.paginator;
    this.getOrderHistory();
    console.log(this.dataSource);
    setInterval(() => {this.getOrderHistory()           
    }, 1000);
   
  }
  /*
  ngAfterViewInit() { // necessary so the ng-if column can be searched
    this.dataSource = new MatTableDataSource();
    this.dataSource.paginator = this.paginator;
    this.getOrderHistory();
    console.log(this.dataSource);
  
  }
  */



getOrderHistory() {
  this.orderService.getOrdersHistory().subscribe((data: Order[]) => {
    // WHEN RELOADING MAY HAVE TO COMPARE AND UPDATE OR SOMETHING
    console.log(data)
    this.dataSource.data = data; // on data receive populate dataSource.data array
    return data;
  });
}

applyFilter(filterValue: string){
  this.dataSource.filter = filterValue.trim().toLowerCase();
}

 // make table sortable: sortable="id" (sort)="onSort($event)"
}
