CREATE SCHEMA VITATRADESCHEMA;
USE VITATRADESCHEMA;

CREATE TABLE VITATRADESCHEMA.stock (
	stock_code VARCHAR(5) NOT NULL,
	stock_name VARCHAR(50) NOT NULL,
	PRIMARY KEY (stock_code)
);

CREATE TABLE VITATRADESCHEMA.orderstatus (
	status_id INT NOT NULL AUTO_INCREMENT,
	status_description VARCHAR(20) NOT NULL,
	PRIMARY KEY (status_id)
);

CREATE TABLE VITATRADESCHEMA.strategy_twomovingavg (
	strategy_id INT NOT NULL,
	stock_code VARCHAR(5) NOT NULL,
	long_avg_duration INT NOT NULL,
	short_avg_duration INT NOT NULL,
	cross_period INT NOT NULL,
	size INT NOT NULL,
	max_amount INT NOT NULL,
	overall_pnl DOUBLE,
	overall_roi DOUBLE,
	is_active BOOLEAN NOT NULL,
	is_sleeping BOOLEAN NOT NULL DEFAULT 0,
	start_time TIMESTAMP NOT NULL,
	exit_time TIMESTAMP,
	PRIMARY KEY (strategy_id),
	FOREIGN KEY (stock_code) REFERENCES stock(stock_code)
);

CREATE TABLE VITATRADESCHEMA.strategy_pricebreakout (
	strategy_id INT NOT NULL,
	stock_code VARCHAR(5) NOT NULL,
	window_period INT NOT NULL,
	trigger_threshold INT NOT NULL,
	size INT NOT NULL,
	max_amount INT NOT NULL,
	overall_pnl DOUBLE,
	overall_roi DOUBLE,
	isactive BOOLEAN NOT NULL,
	is_sleeping BOOLEAN NOT NULL DEFAULT 0,
	start_time TIMESTAMP NOT NULL,
	exit_time TIMESTAMP,
	PRIMARY KEY (strategy_id),
	FOREIGN KEY (stock_code) REFERENCES stock(stock_code)
);

CREATE TABLE VITATRADESCHEMA.strategy_bollingerbands (
	strategy_id INT NOT NULL,
	stock_code VARCHAR(5) NOT NULL,
	average_period INT NOT NULL,
	multiple_of_sd INT NOT NULL,
	size INT NOT NULL,
	max_amount INT NOT NULL,
	overall_pnl DOUBLE,
	overall_roi DOUBLE,
	isactive BOOLEAN NOT NULL,
	is_sleeping BOOLEAN NOT NULL DEFAULT 0,
	start_time TIMESTAMP NOT NULL,
	exit_time TIMESTAMP,
	PRIMARY KEY (strategy_id),
	FOREIGN KEY (stock_code) REFERENCES stock(stock_code)
);

CREATE TABLE VITATRADESCHEMA.orderbook (
	order_id INT NOT NULL AUTO_INCREMENT,
	isbuy BOOLEAN NOT NULL,
	price DOUBLE NOT NULL,
	initial_size INT NOT NULL,
	approved_size INT,
	total DOUBLE NOT NULL,
	order_time TIMESTAMP NOT NULL,
	stock_code VARCHAR(5) NOT NULL,
	status_id INT NOT NULL,
	strategy_id INT NOT NULL,
	PRIMARY KEY (order_id),
	FOREIGN KEY (stock_code) REFERENCES stock(stock_code),
	FOREIGN KEY (status_id) REFERENCES orderstatus(status_id)
);



-- POPULATE DATA INTO ORDERSTATUS TATBLE --
INSERT INTO orderstatus (status_description) VALUES('PENDING');
INSERT INTO orderstatus (status_description) VALUES('FILLED');
INSERT INTO orderstatus (status_description) VALUES('PARTIALLY_FILLED');
INSERT INTO orderstatus (status_description) VALUES('REJECTED');
